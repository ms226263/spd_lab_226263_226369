package com.company;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;



public class Main {


    private static void sortR(RPQ tab[]){
        RPQ temp;
        int zmiana = 1;
        while(zmiana > 0){
            zmiana = 0;
            for(int i=0; i<tab.length-1; i++){
                if(tab[i].getR()>tab[i+1].getR()){
                    temp = tab[i+1];
                    tab[i+1] = tab[i];
                    tab[i] = temp;
                    zmiana++;
                }
            }
        }
        for(int i=0; i<tab.length; i++){
            tab[i].show();
        }
    }

    static int minC = 64000;
    static String permKolejnosc = "";


    static void permutacje(int k, int rozmiar, RPQ P[]){

        RPQ temp = new RPQ();

        if(k==1) {

            for(int i = 0; i<rozmiar;i++){

                int temp2 = RPQ.obliczCzas(P,rozmiar);
                if(temp2<minC){

                    minC = temp2;
                    //System.out.println("MINC:" + minC);
                    //System.out.print("\n");
                    permKolejnosc = "";
                    for(int j = 0; j<rozmiar;j++){
                        permKolejnosc += String.valueOf(P[j].getIndex()+1) + " ";
                    }
                    //System.out.println(permKolejnosc);
                   // System.out.print("\n");

                }

            }


        }
        else {
            for(int i=0;i<k;i++){
                temp = P[i];
                P[i]=P[k-1];
                P[k-1]=temp;
                permutacje(k-1,rozmiar,P);
                temp = P[i];
                P[i]=P[k-1];
                P[k-1]=temp;
            }
        }

    }

    public static void main(String[] args) throws FileNotFoundException{

        File file = new File("rpq.txt");
        Scanner in = new Scanner(file);

        String bufor = in.nextLine();
        int lZadan = Integer.parseInt(bufor);
        RPQ[] rpq = new RPQ[lZadan];
        RPQ[] rpqSortR = new RPQ[lZadan];

        System.out.println("########ODCZYTANE DANE:#################");
        for(int i = lZadan; i>0;i--) {

            bufor = in.nextLine();
            System.out.println(bufor);
            String[] tmp = bufor.split(" ");

            rpq[lZadan-i] = new RPQ();
            rpq[lZadan-i].setR(Integer.parseInt(tmp[0]));
            rpq[lZadan-i].setP(Integer.parseInt(tmp[1]));
            rpq[lZadan-i].setQ(Integer.parseInt(tmp[2]));
            rpq[lZadan-i].setIndex(lZadan-i);
            rpqSortR[lZadan-i] = rpq[lZadan-i];
        }

        System.out.println("\nWynik SortR:\n#Obliczona kolejność zadań#");
        sortR(rpqSortR);

        System.out.println("-----------------------------");
        for(int i = 0 ; i<lZadan ; i++){

            System.out.print(" " + (rpqSortR[i].getIndex()+1));

        }

        //Obliczanie czasu wykonywania
        int c = RPQ.obliczCzas(rpqSortR,lZadan);
        System.out.println("\n\n#Czas wykonywania:#");
        System.out.println(c);




        permutacje(lZadan,lZadan,rpq);
        RPQ[] rpqPerm = new RPQ[lZadan];
        String[] tmp = permKolejnosc.split(" ");

        for(int i = 0 ; i<lZadan ; i++){

            rpqPerm[i]=rpq[Integer.parseInt(tmp[i])-1];

        }

        System.out.println("########NAJLEPSZA PERMUTACJA#################");

        for(int i = 0 ; i<lZadan ; i++){

            System.out.print(" " + (rpqPerm[i].getIndex()+1));

        }
        System.out.println("");
        for(int i = 0 ; i<lZadan ; i++){

            rpqPerm[i].show();

        }
        System.out.println("czas:");
        c = RPQ.obliczCzas(rpqPerm,lZadan);
        System.out.println(c);
        System.out.println("########SCHRAGE#################");
        RPQ[] schrage = new RPQ[lZadan];
        schrage = RPQ.Schrage(rpq,lZadan);
        System.out.println("Kolejnosc Schrage: ");
        for(int i = 0 ; i<lZadan ; i++){

            System.out.print(" " + (schrage[i].getIndex()+1));

        }
        System.out.println("");
        for(int i = 0 ; i<lZadan ; i++){

            schrage[i].show();

        }
        c = RPQ.obliczCzas(schrage,lZadan);
        System.out.println("\nCzas Schrage: " + c);



    }
}
