package com.company;

import java.util.LinkedList;
import java.util.List;

public class RPQ {

    private int R;
    private int P;
    private int Q;
    private int index;

    RPQ(){
        R = 0;
        Q = 0;
        P = 0;

    }

    public int getR() {
        return R;
    }

    public int getP() {
        return P;
    }

    public int getQ() {
        return Q;
    }

    public int getIndex() {
        return index;
    }

    public void setR(int R) {
        this.R = R;
    }

    public void setP(int P) {
        this.P = P;
    }

    public void setQ(int Q) {
        this.Q = Q;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public void show() {
        System.out.println((index+1)+ ". " + R + " " + P + " " + Q);
    }

    public static int obliczCzas(RPQ[] rpq,int lZadan) {
        int t = 0;
        int c = 0;
        for(int i = 0 ; i<lZadan ; i++){

            //R+P
            if(t >= rpq[i].getR()) t = t+rpq[i].getP();
            else t = rpq[i].getR() + rpq[i].getP();

            if(t+rpq[i].getQ() > c) c = t+rpq[i].getQ();

        }
        return c;
    }

    public static int findMinR(List<RPQ> lista){
        int minR = lista.get(0).getR();
        int index = 0;
        for(int i=0; i<lista.size();i++){
            if(lista.get(i).getR() < minR)
            {
                minR = lista.get(i).getR();
                index = i;
            }
        }
        return index;
    }

    public static int findMaxQ(List<RPQ> lista){
        int maxQ = lista.get(0).getQ();
        int index = 0;
        for(int i=0; i<lista.size();i++){
            if(lista.get(i).getQ() > maxQ)
            {
                maxQ = lista.get(i).getQ();
                index = i;
            }
        }
        return index;
    }

    public static RPQ[] Schrage(RPQ[] tab, int rozmiar)
    {
        int mainI = 1;
        RPQ[] wynik = new RPQ[rozmiar];
        List<RPQ> G = new LinkedList<>();
        List<RPQ> N = new LinkedList<>();
        int t = 0;

        for(int i=0; i< rozmiar; i++)
        {
            N.add(tab[i]);
        }
        t = findMinR(N);

        while(!G.isEmpty() || !N.isEmpty())
        {
            while(!N.isEmpty() && findMinR(N) <=t)
            {
                RPQ tempJ = N.get(findMinR(N));
                G.add(tempJ);
                N.remove(tempJ);
            }
            if(G.isEmpty())
            {
                int tempIndex = findMinR(N);
                t= N.get(tempIndex).getR();
            }
            else {
                RPQ tempJ = G.get(findMaxQ(G));
                G.remove(tempJ);
                wynik[mainI-1] = tempJ;
                mainI += 1;
                t=t+tempJ.getP();
            }

        }
        return wynik;
    }


}
