Algorytm Schrage. Porównianie szukaniem permutacji i SortR.
Algorytm znajduje się w klasie RPQ.
Dane 1:
7
8354 1 5507
8455 696 512
2900 435 8619
6176 424 3688
586 971 76
7751 134 5877
7935 516 3017

SortR: 5 3 4 6 7 1 2 
Czas wykonywania: 13959
Najlepsza permutacja: 3 4 5 6 1 7 2
Czas wykonywania: 13862
Schrage: 3 6 1 4 7 2 5
Czas wykonywania: 13862

Dane 2:

7
0 1 5507
0 696 512
0 435 8619
0 424 3688
0 971 76
0 134 5877
0 516 3017

SortR: 1 2 3 4 5 6 7 
Czas wykonywania: 9751
Najlepsza permutacja: 3 2 4 5 6 7 1
Czas wykonywania: 9054
Schrage: 3 6 1 4 7 2 5
Czas wykonywania: 9054

Dane 3:

10
8354 1 5507
8455 696 512
2900 435 8619
6176 424 3688
586 971 76
7751 134 5877
7935 516 3017
586 971 76
7751 134 5877
7935 516 3017

SortR: 5 8 3 4 6 9 7 10 1 2
Czas wykonywania: 14559
Najlepsza permutacja: 8 3 5 9 6 1 7 4 10 2
Czas wykonywania: 13896
Schrage: 5 3 6 9 1 4 7 10 2 8
Czas wykonywania: 13896

Dane 4: 

12
8354 1 5507
8455 696 512
2900 435 8619
6176 424 3688
586 971 76
7751 134 5877
7935 516 3017
586 971 76
7751 134 5877
7935 516 3017
12 443 111
100 1 13

SortR: 11 12 5 8 3 4 6 9 7 10 1 2
Czas wykonywania: 14559
Najlepsza permutacja: 8 3 5 9 6 1 7 4 10 11 12 2
Czas wykonywania: 13896
Schrage: 3 6 9 1 4 7 10 2 11 5 8 12
Czas wykonywania: 13896

Dane 5

4
1 4 7
3 3 1
10 9 2
7 7 7

SortR: 1 2 4 3
Czas wykonywania: 26
Najlepsza permutacja: 1 2 4 3
Czas wykonywania: 26
Schrage: 1 4 3 2
Czas wykonywania: 27

Wnioski:

Dla większej ilości danych algorytm Schrage wykonuje się znacznie szybciej niż szukanie najlepszej permutacji. W większości przypadków znaleziona kolejność jest taka sama.
SortR zwraca gorsze wyniki od poprzednich algorytmów. W jednym przypadku Schrage zwrócił gorszy wynik od pozostałych metod (Dane 5).
Wszystkie wyniki były sprawdzane przy pomocy programu rpq.tester udostępnionego przez dr Makuchowskiego.