package com.company;

public class RPQ {

    private int R;
    private int P;
    private int Q;
    private int index;

    RPQ(){
        R = 0;
        Q = 0;
        P = 0;

    }

    public int getR() {
        return R;
    }

    public int getP() {
        return P;
    }

    public int getQ() {
        return Q;
    }

    public int getIndex() {
        return index;
    }

    public void setR(int R) {
        this.R = R;
    }

    public void setP(int P) {
        this.P = P;
    }

    public void setQ(int Q) {
        this.Q = Q;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public void show() {
        System.out.println(index+ ". " + R + " " + P + " " + Q);
    }

    public static int obliczCzas(RPQ[] rpq,int lZadan) {
        int t = 0;
        int c = 0;
        for(int i = 0 ; i<lZadan ; i++){

            //R+P
            if(t >= rpq[i].getR()) t = t+rpq[i].getP();
            else t = rpq[i].getR() + rpq[i].getP();

            if(t+rpq[i].getQ() > c) c = t+rpq[i].getQ();

        }
        return c;
    }


}
