package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Main {

    public static int obliczCzas(Zadanie[] z,int lZadan)
    {
        int t1=0;
        int t2=0;
        for(int i=0;i<lZadan;i++){
            t1+=z[i].getCzas();
            if(t1>t2)t2=t2+(t1-t2)+z[i].getCzas1();
            else t2+=z[i].getCzas1();
        }

        return t2;

    }

    static int minC = 64000;
    static String permKolejnosc = "";


    static void permutacje(int k, int rozmiar, Zadanie P[]){
        Zadanie temp = new Zadanie();
        if(k==1) {

            for(int i = 0; i<rozmiar;i++){

                int temp2 = obliczCzas(P,rozmiar);
                if(temp2<minC){

                    minC = temp2;
                    permKolejnosc = "";
                    for(int j = 0; j<rozmiar;j++){
                        permKolejnosc += String.valueOf(P[j].getIndex()+1) + " ";
                    }

                }

            }


        }
        else {
            for(int i=0;i<k;i++){
                temp = P[i];
                P[i]=P[k-1];
                P[k-1]=temp;
                permutacje(k-1,rozmiar,P);
                temp = P[i];
                P[i]=P[k-1];
                P[k-1]=temp;
            }
        }

    }

    public static void main(String[] args) throws FileNotFoundException {

        File file = new File("flowshop.txt");
        Scanner in = new Scanner(file);

        String bufor = in.nextLine();
        int lZadan = Integer.parseInt(bufor);
        Zadanie[] zadania = new Zadanie[lZadan];


        System.out.println("########ODCZYTANE DANE:#################");
        for(int i = lZadan; i>0;i--) {

            bufor = in.nextLine();
            System.out.println(bufor);
            String[] tmp = bufor.split(" ");

            zadania[lZadan-i] = new Zadanie();
            zadania[lZadan-i].setCzas(Integer.parseInt(tmp[0]));
            zadania[lZadan-i].setCzas1(Integer.parseInt(tmp[1]));
            zadania[lZadan-i].setIndex(lZadan-i);
        }

        System.out.println("########ODCZYTANE DANE:#################");
        for(int i = 0 ; i<lZadan ; i++)
        {
            System.out.print(zadania[i].getCzas() + " ");
            System.out.print(zadania[i].getCzas1() + " ");
            System.out.print(zadania[i].getIndex() + "\n");
        }
/////////////////////////////////////////////////////////////////////////////////////////////

        permutacje(lZadan,lZadan,zadania);
        Zadanie[] flowPerm = new Zadanie[lZadan];
        String[] tmp = permKolejnosc.split(" ");

        for(int i = 0 ; i<lZadan ; i++){

            flowPerm[i]=zadania[Integer.parseInt(tmp[i])-1];

        }

        System.out.println("########NAJLEPSZA PERMUTACJA#################");

        for(int i = 0 ; i<lZadan ; i++){

            System.out.print(" " + (flowPerm[i].getIndex()+1));

        }
        System.out.println("");
        for(int i = 0 ; i<lZadan ; i++){

            flowPerm[i].show();

        }
        System.out.println("czas:");
        int c = obliczCzas(flowPerm,lZadan);
        System.out.println(c);

    }
}
