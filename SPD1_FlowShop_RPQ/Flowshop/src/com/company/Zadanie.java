package com.company;

public class Zadanie {

    private int czas;
    private int czas1;
    private int index;


    Zadanie(){
        czas = 0;
        czas1=0;
    }

    Zadanie(int t,int t3){
        czas = t;
        czas1 = t3;

    }

    public int getCzas() {
        return czas;
    }

    public void setCzas(int czas) {
        this.czas = czas;
    }
    public int getCzas1() {
        return czas1;
    }

    public void setCzas1(int czas1) {
        this.czas1 = czas1;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public void show() {
        System.out.println(index+ ". " + czas + " " + czas1);
    }
}
