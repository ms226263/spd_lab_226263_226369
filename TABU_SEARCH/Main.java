package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static int obliczCzas( List<Zadanie> z,int lZadan)
    {
        int t1=0;
        int t2=0;
        int t3=0;
        for(int i=0;i<lZadan;i++){
            t1+=z.get(i).getCzas();
            if(t1>t2)t2=t2+(t1-t2)+z.get(i).getCzas1();
            else t2+=z.get(i).getCzas1();
            if(t3<t2)t3 = t3 + (t2 - t3) + z.get(i).getCzas2();
            else t3 += z.get(i).getCzas2();
        }
        return t3;
    }

    static void ts (int k, int rozmiar, List<Zadanie> P){
        Zadanie temp ;
        Random generator = new Random();
        List<Zadanie> temp1 = new LinkedList<Zadanie>();
        List<Zadanie> temp2 = new LinkedList<Zadanie>();
        List<Zadanie> temp3 = new LinkedList<Zadanie>();
        List<tabu> list_tabu = new LinkedList<tabu>();
        tabu tabu_tmp;
        Zadanie atrybut1 = new Zadanie();
        int atrybut2=99999;

        System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        temp1 = P;
        int a;
        // Generowanie pierwszego losowego rozwiazania
        while(!P.isEmpty()){
            a = generator.nextInt(P.size());
            temp2.add(temp1.get(a));
            temp1.remove(a);
        }

        for (int c = 0; c < temp2.size(); c++) {
            temp2.get(c).show();
        }
        System.out.println("czas dla pierwszego rozwiazania=" +obliczCzas(temp2,temp2.size()));
        for (int c = 0; c < temp2.size(); c++) {
            temp3.add(temp2.get(c));
        }
        ////WARUNEK ZAKONCZENIA
        boolean check1 = true;
        //////////////////////////////////////////////////////////////
        while (check1) {
            check1 = false;
            int s = obliczCzas(temp3, temp3.size());
            temp = temp3.get(0);
            tabu_tmp = new tabu();
            temp3.remove(0);
            ////////////////////////////////////////////////////////////////////////
            for (int i = temp3.size(); i >= 0; i--) {

                temp3.add(temp3.size() - i, temp);
                for (int c = 0; c < temp3.size(); c++) {
                    temp3.get(c).show();
                }
                System.out.println();
                ////////////////////////////////////////////////////////////
                if (obliczCzas(temp3, temp3.size()) < s) {
                    check1 = true;
                    s = obliczCzas(temp3, temp3.size());
                    // wprowadzenie atrybutow do tabu tmp
                    tabu_tmp.setZadanie1(temp);
                    tabu_tmp.setId_zadania(temp3.size() - i);
                    /// jezeliu lista pusta to dodaj ruch do listy tabu, i przypisz rozwiazanie do x opt
                    if((list_tabu.size()==0)){
                        list_tabu.add(tabu_tmp);
                        while (temp2.size() != 0) {
                            temp2.remove(0);
                        }
                        for (int c = 0; c < temp3.size(); c++) {
                            temp2.add(temp3.get(c));
                            temp2.get(c).show();
                        }
                    }
                    else/// jezeli nie jest pusta
                        {
                        for(int j=0;j<list_tabu.size();j++) { // przeszukiwanie listy tabu z poszukaniu takiego samego ruchu
                            if ((list_tabu.get(j).getId_zadania() == temp3.size() - i) && (list_tabu.get(j).getZadanie1() == temp)) {
                                // jezeli jest to zapisz to zadanie i miejsce gdzie byl wstawiany
                                atrybut1 = temp;
                                atrybut2 = temp3.size() - i;
                            }
                        }
                        // jezeli nie ma takiego ruchu to wstaw go do listy i przypisz x opt
                            if((atrybut1 != temp)&&(atrybut2 != temp3.size() - i)){
                                list_tabu.add(tabu_tmp);
                                while (temp2.size() != 0) {
                                    temp2.remove(0);
                                }
                                for (int c = 0; c < temp3.size(); c++) {
                                    temp2.add(temp3.get(c));
                                    temp2.get(c).show();
                                }
                            }
                        }

                }
                temp3.remove(temp);
            }
            // wyswietlanie listy tabu
            System.out.println("lista tabu :");
            for (int j=0;j<list_tabu.size();j++){
                System.out.println("ruch : " +list_tabu.get(j).getZadanie1() + "  id  "+list_tabu.get(j).getId_zadania());
                list_tabu.get(j).kadencja--;
                if ((list_tabu.get(j).kadencja--)==0)list_tabu.remove(j);
            }
            while (temp3.size() != 0) {
                temp3.remove(0);
            }
            for (int c = 0; c < temp2.size(); c++) {
                temp3.add(temp2.get(c));
            }
        }
        for (int c = 0; c < temp3.size(); c++) {
            temp2.get(c).show();
        }
        System.out.println("czas = "+obliczCzas(temp2,temp2.size()));
    }

    public static void main(String[] args) throws FileNotFoundException{
        File file = new File("flowshop.txt");
        Scanner in = new Scanner(file);

        String bufor = in.nextLine();
        int lZadan = Integer.parseInt(bufor);
        Zadanie[] zadania = new Zadanie[lZadan];

        System.out.println("########################ODCZYTANE DANE:###########################");
        for(int i = lZadan; i>0;i--) {

            bufor = in.nextLine();
            //System.out.println(bufor);
            String[] tmp = bufor.split(" ");

            zadania[lZadan-i] = new Zadanie();
            zadania[lZadan-i].setCzas(Integer.parseInt(tmp[0]));
            zadania[lZadan-i].setCzas1(Integer.parseInt(tmp[1]));
            zadania[lZadan-i].setCzas2(Integer.parseInt(tmp[2]));
            zadania[lZadan-i].setIndex(lZadan-i);
        }
        System.out.println("########ODCZYTANE DANE:#################");
        for(int i = 0 ; i<lZadan ; i++)
        {
            System.out.print(zadania[i].getCzas() + " ");
            System.out.print(zadania[i].getCzas1() + " ");
            System.out.print(zadania[i].getCzas2() + " ");
            System.out.print(zadania[i].getIndex() + "\n");
        }
            ///////////////////////////////////////////////////////////////////
        List<Zadanie> zad = new LinkedList<Zadanie>();
        for (int i =0;i<zadania.length;i++){
            zad.add(zadania[i]);
        }

        ts(lZadan,lZadan,zad);
    }
}
