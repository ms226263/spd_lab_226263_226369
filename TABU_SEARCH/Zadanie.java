package com.company;

public class Zadanie {
    private int czas;
    private int czas1;
    private int czas2;
    private int index;


    Zadanie(){
        czas = 0;
        czas1=0;
        czas2 = 0;
    }

    Zadanie(int t,int t3,int t4){
        czas = t;
        czas1 = t3;
        czas2 = t4;

    }

    public int getCzas() {
        return czas;
    }

    public void setCzas(int czas) {
        this.czas = czas;
    }
    public int getCzas1() {
        return czas1;
    }

    public void setCzas1(int czas1) {
        this.czas1 = czas1;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getCzas2() {
        return czas2;
    }
    public void setCzas2(int czas2) {
        this.czas2 = czas2;
    }

    public void show() {
        System.out.println((index+1)+ ". " + czas + " " + czas1 + " " + czas2);
    }

    @Override
    public String toString() {
        return (index+1)+ ". " + czas + " " + czas1 + " " + czas2;
    }
}
