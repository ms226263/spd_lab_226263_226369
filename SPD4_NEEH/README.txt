Algorytm NEEH 
Dane to kolumny kolejno : czas1, czas2, czas3.
wiersze to zadania do wykonania.
Dane 1:
4 1 4
4 3 3
1 2 3
5 1 3

czas = 18 Kolejnosc : 3124

Dane 2 : 
4 1 4
4 3 3
1 2 3
5 1 3
5 6 7
3 2 1
6 7 8
3 5 7

czas = 42 Kolejnosc : 38576124

Dane 3:
4 1 4
4 3 3
1 2 3
5 1 3
5 6 7
3 2 1
6 7 8
3 5 7
2 2 2
5 5 5
1 7 3
1 1 1
5 3 7
3 1 5
7 4 1

czas = 64 Kolejnosc : 14,12,9,3,13,8,10,5,7,15,11,6,1,2,4
Wnioski:
Algorytm sortuje zadania zgodnie z zalozeniem, co sprawdzilismy na przykladzie z opisu lab. 4, czyli na danych nr 1.
Zlozonosc obliczenia nie jest skomplikowana i nie wymaga duzo czasu w porownaniu do na przyklad przegladu zupelnego,
przez co dla zadan z duza iloscia danych jest to lepsze rozwiazanie.
Algorytm nie jest trudny do zrozumienia i do zaimplementowania.
